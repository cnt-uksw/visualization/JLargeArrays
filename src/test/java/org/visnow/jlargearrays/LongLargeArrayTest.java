/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class LongLargeArrayTest extends LargeArrayTest
{

    public LongLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testEmptyLongLargeArray()
    {
        LongLargeArray a = new LongLargeArray(0);
        assertEquals(0, a.length());
        Throwable e = null;
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e = null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        
        a = new LongLargeArray(0, 1, true);
        assertEquals(0, a.length());
        assertTrue(a.isConstant());
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e= null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
    }

    @Test
    public void testLongLargeArrayEqualsHashCode()
    {
        LongLargeArray a = new LongLargeArray(10);
        LongLargeArray b = new LongLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.setLong(0, 1);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testLongLargeArrayApproximateHashCode()
    {
        LongLargeArray a = new LongLargeArray(10);
        LongLargeArray b = new LongLargeArray(10);
        a.setLong(0, 1);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new LongLargeArray(10, 0l, true);
        b = new LongLargeArray(10, 1l, true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testSerialization()
    {
        long size = 1 << 5;
        LongLargeArray a = new LongLargeArray(size);
        LongLargeArray b = null;
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (LongLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        a = new LongLargeArray(size, 2l, true);
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (LongLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
    }

    @Test
    public void testLongLargeArrayConstant()
    {
        LongLargeArray a = new LongLargeArray(10, 2, true);
        assertTrue(a.isConstant());
        assertEquals(2, a.getLong(0));
        assertEquals(2, a.getLong(a.length() - 1));
        a.setLong(0, 3);
        assertEquals(3, a.getLong(0));
        assertFalse(a.isConstant());
    }

    @Test
    public void testLongLargeArrayGetSet()
    {
        LongLargeArray a = new LongLargeArray(10);
        long idx = 5;
        int val = -100;
        a.setLong(idx, val);
        assertEquals(val, a.getLong(idx));
        idx = 6;
        a.set(idx, val);
        assertEquals(val, (a.get(idx)).longValue());
    }

    @Test
    public void testLongLargeArrayGetSetNative()
    {
        LongLargeArray a = new LongLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            long val = -100;
            a.setToNative(idx, val);
            assertEquals(val, (long) a.getFromNative(idx));
        }
    }

    @Test
    public void testLongLargeArrayGetData()
    {
        long[] data = new long[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        LongLargeArray a = new LongLargeArray(data);
        long[] res = a.getLongData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++]);
        }
    }
}
