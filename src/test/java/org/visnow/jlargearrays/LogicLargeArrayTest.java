/*
 * JLargeArrays
 * Copyright (C) 2006-2019 University of Warsaw, ICM
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jlargearrays;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Piotr Wendykier (p.wendykier@icm.edu.pl)
 */
public class LogicLargeArrayTest extends LargeArrayTest
{

    public LogicLargeArrayTest(boolean useOffHeapMemory)
    {
        super(useOffHeapMemory);
    }

    @Test
    public void testEmptyLogicLargeArray()
    {
        LogicLargeArray a = new LogicLargeArray(0);
        assertEquals(0, a.length());
        Throwable e = null;
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e= null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        
        a = new LogicLargeArray(0, (byte)1, true);
        assertEquals(0, a.length());
        assertTrue(a.isConstant());
        try {
            a.get_safe(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
        e= null;
        try {
            a.set_safe(0, 0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            e = ex;
        }
        assertTrue(e instanceof ArrayIndexOutOfBoundsException);
    }
    
    
    @Test
    public void testLogicLargeArrayEqualsHashCode()
    {
        LogicLargeArray a = new LogicLargeArray(10);
        LogicLargeArray b = new LogicLargeArray(10);
        assertTrue(a.equals(a));
        assertTrue(a.hashCode() == a.hashCode());
        assertTrue(a.hashCode() == a.hashCode(1f));
        assertTrue(a.equals(b));
        assertTrue(a.hashCode() == b.hashCode());
        assertTrue(a.hashCode() == b.hashCode(1f));
        a.setBoolean(0, true);
        assertFalse(a.equals(b));
        assertFalse(a.hashCode() == b.hashCode());
    }

    @Test
    public void testLogicLargeArrayApproximateHashCode()
    {
        LogicLargeArray a = new LogicLargeArray(10);
        LogicLargeArray b = new LogicLargeArray(10);
        a.setBoolean(0, true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
        a = new LogicLargeArray(10, (byte) 0, true);
        b = new LogicLargeArray(10, (byte) 1, true);
        assertTrue(a.hashCode(0f) == b.hashCode(0f));
        assertFalse(a.hashCode(1f) == b.hashCode(1f));
    }

    @Test
    public void testSerialization()
    {
        long size = 1 << 5;
        LogicLargeArray a = new LogicLargeArray(size);
        LogicLargeArray b = null;
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (LogicLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
        a = new LogicLargeArray(size, (byte) 1, true);
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(); ObjectOutputStream o = new ObjectOutputStream(bout)) {
            o.writeObject(a);
            o.close();
            try (ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bout.toByteArray()))) {
                b = (LogicLargeArray) in.readObject();
            }
        } catch (IOException | ClassNotFoundException ex) {
            fail(ex.getMessage());
        }
        assertTrue(a.equals(b));
    }

    @Test
    public void testLogicLargeArrayConstant()
    {
        LogicLargeArray a = new LogicLargeArray(10, (byte) 1, true);
        assertTrue(a.isConstant());
        assertTrue(a.getBoolean(0));
        assertTrue(a.getBoolean(a.length() - 1));
        a.setBoolean(0, false);
        assertFalse(a.getBoolean(0));
        assertFalse(a.isConstant());
    }

    @Test
    public void testLogicLargeArrayGetSet()
    {
        LogicLargeArray a = new LogicLargeArray(10);
        long idx = 5;
        byte val = 1;
        a.set(idx, val);
        assertEquals(val, a.getByte(idx));
        idx = 6;
        a.set(idx, val);
        assertEquals(val, (byte) a.get(idx));
    }

    @Test
    public void testLogicLargeArrayGetSetNative()
    {
        LogicLargeArray a = new LogicLargeArray(10);
        if (a.isLarge()) {
            long idx = 5;
            Byte val = 1;
            a.setToNative(idx, val);
            assertEquals(val, a.getFromNative(idx));
        }
    }

    @Test
    public void testLogicLargeArrayGetData()
    {
        boolean[] data = new boolean[]{true, false, false, false, true, true, true, false, true, true};
        long startPos = 2;
        long endPos = 7;
        long step = 2;
        LogicLargeArray a = new LogicLargeArray(data);
        boolean[] res = a.getBooleanData(null, startPos, endPos, step);
        int idx = 0;
        for (long i = startPos; i < endPos; i += step) {
            assertEquals(data[(int) i], res[idx++]);
        }
    }

    @Test
    public void testLogicLargeArrayNot()
    {
        LogicLargeArray a = new LogicLargeArray(10);
        long idx = 5;
        boolean val = true;
        a.setBoolean(idx, val);
        LogicLargeArray b = a.not();
        for (int i = 0; i < a.length; i++) {
            assertEquals(1 - a.getByte(i), b.getByte(i));
        }
    }

    @Test
    public void testLogicLargeArrayXor()
    {
        LogicLargeArray a = new LogicLargeArray(10);
        long idx = 5;
        boolean val = true;
        a.setBoolean(idx, val);
        LogicLargeArray b = a.xor(a);
        for (int i = 0; i < a.length; i++) {
            assertEquals(a.getByte(i) ^ a.getByte(i), b.getByte(i));
        }
    }

    @Test
    public void testLogicLargeArrayOr()
    {
        LogicLargeArray a = new LogicLargeArray(10);
        long idx = 5;
        boolean val = true;
        a.setBoolean(idx, val);
        LogicLargeArray b = a.or(a);
        for (int i = 0; i < a.length; i++) {
            assertEquals(a.getByte(i) | a.getByte(i), b.getByte(i));
        }
    }

    @Test
    public void testLogicLargeArrayAnd()
    {
        LogicLargeArray a = new LogicLargeArray(10);
        long idx = 5;
        boolean val = true;
        a.setBoolean(idx, val);
        LogicLargeArray b = a.and(a);
        for (int i = 0; i < a.length; i++) {
            assertEquals(a.getByte(i) & a.getByte(i), b.getByte(i));
        }
    }
}
